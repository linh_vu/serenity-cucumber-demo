package vn.mycompany.auto.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.WebElement;

@DefaultUrl("https://accounts.google.com")
public class LoginPage extends PageObject{
	
	@FindBy(id="Email")
	WebElement emailField;
	
	@FindBy(id="errormsg_0_Passwd")
	WebElement errMsg;
	
	@FindBy(id="signIn")
	WebElement signInBtn;

	public void enter_email(String email) {
		emailField.sendKeys(email);
		
	}

	public String getErrMsg() {
		return errMsg.getText();
	}

	public void clickLoginBtn() {
		signInBtn.click();
		
	}

}
