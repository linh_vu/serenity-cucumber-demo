package vn.mycompany.auto.cucumber;

import net.thucydides.core.annotations.Steps;

import org.junit.Assert;

import vn.mycompany.auto.steps.LoginSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginScenarioSteps {
	
	@Steps
	LoginSteps loginStep;
	
	@Given("^I am on home page$")
	public void i_am_on_home_page() throws Throwable {
		loginStep.open_home_page();
	}

	@When("^I input my account is \"(.*?)\"$")
	public void i_input_my_account_is(String email) throws Throwable {
		loginStep.input_email(email);
	}
	
	@When("^I click on login button$")
	public void i_click_on_login_button() throws Throwable {
		loginStep.click_on_login_btn();
	}

	@Then("^I should see a alert message is \"(.*?)\"$")
	public void i_should_see_a_alert_message_is(String msgMrg) throws Throwable {
	  Assert.assertEquals(loginStep.getAlertMsg(), msgMrg);
	}

}
